// Requisito: babel/plugin-proposal-object-rest-spread

// Desestruturação possibilita “desempacotar” valores de um array ou propriedades de objetos, em variáveis diferentes!
// Rest pega o resto dos parametros e coloca em uma variavel
// SPREAD repassa os valores

// Desestruturação
const arr = [1, 2, 3];
const a, b, c = arr;
// a=1, b=2, c=3

// Rest
const arr = [1, 2, 3];
const a, ..b = arr;
// a=1, b=[2,3]

// SPREAD
const arr1 = [1, 2, 3];
const arr2 = [4, 5, 6];
const arr3 = [...arr1, ...arr2];
// arr3 = [1, 2, 3, 4, 5, 6]


// Exemplos praticos
// Rest
function soma(a, b, ...params) {
	return params;
}

// SPREAD
const usuario = {
	nome:'Diego',
	idade: 23,
	empresa: 'Bisnaga SA',
};

const usuario2 = {...usuario1, nome: 'Igor'};

// usuario2 = {nome: 'Igor', idade:23, empresa: 'Bisnaga SA'}

