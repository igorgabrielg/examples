// Sintaxe curta de objeto 

const nome = 'Igor';
const idade = 21;

const usuario = {
	nome: nome,
	idade: idade,
	empresa: 'Bisnaga SA',
};

// Quando o nome da variavel e o mesmo que da chave e possivel colocar apenas o nome da variavel na propiedade

const usuarioCurto = {
	nome,
	idade,
	empresa: 'Bisnaga SA',
};