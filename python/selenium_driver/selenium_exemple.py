from selenium import webdriver
import os

path = os.path.join(os.path.abspath(''), 'driver', 'chromedriver.exe')
driver = webdriver.Chrome(executable_path=path)


def test_page(url):
    # Connect page
    driver.get(url)

    # Get URL page
    print(driver.current_url)

    # Get code page
    print(driver.page_source)


test_page('http://google.com')