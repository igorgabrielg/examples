import pandas as pd

df = pd.read_csv('csv_exemple.csv')
print(f'Dataframe do CSV:\n{df}')

# Imprime item por item da coluna link do dataframe
print('Links:')
for i in df['link']:
    print(i)

# Cria um dicionario
data = {'rede': ['Gmail', 'Youtube', 'Keep'],
        'total_usuarios': ['200000', '300000', '10000'],
        'link': ['http://gmail.com', 'http://youtube.com', 'http://keep.google.com']
        }

# Cria um Dataframe do dicionario
df = pd.DataFrame(data, columns=['rede', 'total_usuarios', 'link'])

print(f'Dataframe do dicionario:\n{df}')