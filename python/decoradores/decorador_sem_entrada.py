import time


# Função decorador
def runtime(func):
    """
    Verifica o tempo de execução de uma função

    :param func:
    :return:
    """

    # função interna
    def interna(*args, **kwargs):
        inicio = time.time()
        func(*args, **kwargs)
        fim = time.time()
        print(f'A execução iniciou em: {inicio}')
        print(f'A execução finalizou em: {fim}')
        print(f'A execução demorou em: {fim - inicio}')

    return interna

# Função decorador recebe a função imprima
@runtime
def imprima():
    """
    Imprime de 1 ate 45000

    :return:
    """
    for i in range(1, 45000):
        print(i)


imprima()