def valida_type(type):
    """
    Função valida de os tipos estão corretos
    
    :param type:
    :return:
    """
    # Função Interna
    def valida(func):
        def inner(*args):
            if all(isinstance(val, type) for val in args):
                return func(*args)

        return inner

    return valida


@valida_type(int)
def soma_int(x, y):
    return x + y


@valida_type(str)
def unir_str(x, y):
    return x + y


print(soma_int(1, 2))
print(unir_str('ig', 'or'))
print(unir_str('ig', 2))
