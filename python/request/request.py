import requests


# Obtem codigo da pagina
def code_html(site):
    '''
    page code return

    :param site:
    :return:
    '''
    try:
        page = requests.get(site)
        return page.text
    except OSError:
        print("Verifique sua conexão com a internet e tente novamente.")


print(code_html("https://nerdstore.com.br/categoria/especiais/game-of-thrones/"))
