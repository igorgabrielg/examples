package fifa2019;

public class Jogador  implements Comparable<Jogador>{
	public String nome;
	public String pontuacao;
	public String clube;
	public String pais;
	
	// Cria as instancias de Jogador
	public Jogador (String nome, String pontuacao, String clube, String pais) {
		this.nome = nome;
		this.pontuacao = pontuacao;
		this.clube = clube; 
		this.pais = pais;
	}
	
	// Obtem nome
	public String getNome() {
		 return nome;
	 }
	
	// Obtem Pontua��o	
	public String getPontuacao() {
		 return pontuacao;
	 }
	
	// Obtem Clube
	public String getClube() {
		 return clube;
	 }
	
	// Obtem Pais
	public String getPais() {
		 return pais;
	 }
	 
	// Retorna os valores juntos
	public String toString() {
		return "Pontua��o: " + pontuacao + "  Nome: " + nome + "  Clube: "+ clube+ "  Pais: " + pais + "\r\n";
	}

	
	@Override
	public int compareTo(Jogador col) {
		// TODO Auto-generated method stub
		return this.pontuacao.compareTo(col.pontuacao);
	}
	
	
}