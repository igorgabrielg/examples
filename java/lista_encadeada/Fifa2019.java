package fifa2019;

import java.util.*;

public class Fifa2019 {
	
	public static void main(String[] args) {
		
		// Metodo Random gera valores aleatorios
        Random random = new Random();
        // Cria o ArrayList
		List<Jogador> jogadores = new ArrayList<Jogador>();
		
		// Cria as Instancias do objeto Jogador
		jogadores.add(new Jogador("Igor Gabriel", Integer.toString(random.nextInt(10)), "S�o Paulo", "Brasil"));
		jogadores.add(new Jogador("Vinicius Santos", Integer.toString(random.nextInt(10)), "Corinthias", "Brasil"));
		jogadores.add(new Jogador("Patrick Cavalcante", Integer.toString(random.nextInt(10)), "Flamengo", "Polo Norte"));
		jogadores.add(new Jogador("Ramiro Victor", Integer.toString(random.nextInt(10)), "Luzi�nia", "China"));
		jogadores.add(new Jogador("Miguel Silva", Integer.toString(random.nextInt(10)), "Brasilia", "Acre"));
		jogadores.add(new Jogador("Arthur Cavalcante", Integer.toString(random.nextInt(10)), "Acre", "Alemanha"));
		jogadores.add(new Jogador("Davi Santos", Integer.toString(random.nextInt(10)), "Itaiti", "Italia"));
		jogadores.add(new Jogador("Bernado Pereira", Integer.toString(random.nextInt(10)), "Luziania FC", "Portugal"));
		jogadores.add(new Jogador("Antonio Gabriel", Integer.toString(random.nextInt(10)), "Cruzeiro", "Argentina"));
		jogadores.add(new Jogador("Luiz Henrique", Integer.toString(random.nextInt(10)), "Atletico Paranaense", "Portugal"));
		
		// Ordena de forma crescente
		jogadores.sort(Comparator.comparing(Jogador::getPontuacao));
		System.out.println("Rank geral:\n" + jogadores);
		
		// Remove os 5 ultimos jogadores
		for (int i = 0; i <= 4; i = i + 1) {
			jogadores.remove(0);
		}
		
		// Reverte a lista
		Collections.reverse(jogadores);
		
		System.out.println("\n 5 Melhores da Fifa 2019: \n" + jogadores);
	}


}
